﻿using System;
using System.Runtime.Serialization;

namespace SerializationS
{
    [Serializable]
    [DataContract]
    public class Character
    {
        [NonSerialized]
        private readonly Random rnd = new Random();

        [DataMember]
        public int Experience { get; set; }
        [DataMember]
        public int Category { get; set; }
        [DataMember]
        public int Salary { get; set; }
        [DataMember]
        public double PremiumCoefficient { get; set; }
        [DataMember]
        public int Premium { get; set; }
        [DataMember]
        public int TotalSalary { get; set; }

        public Character()
        {
            Experience = rnd.Next(1, 10);
            Category = rnd.Next(1, 6);
            Salary = 25000 + Experience * 500;
            PremiumCoefficient = Math.Round(Category * 0.33, 2);
            Premium = Convert.ToInt32(Salary * PremiumCoefficient);
            TotalSalary = Salary + Premium;
        }

        public Character(int experience, int category, int salary, double premiumCoefficient, int premium, int totalSalary)
        {
            Experience = experience;
            Category = category;
            Salary = salary;
            PremiumCoefficient = premiumCoefficient;
            Premium = premium;
            TotalSalary = totalSalary;
        }

        public override string ToString()
        {
            return $"Стаж: {Experience}\nОплата: {TotalSalary} руб.\n";
        }
    }
}
