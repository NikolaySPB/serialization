﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Xml.Serialization;

namespace SerializationS
{
    public class SaveSerialization
    {
        private void Color (string text)
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine(text + "\n");
            Console.ResetColor();
        }
        
        public void SaveBinaryFormatter(List<Person> collection)
        {
            Color("BinaryFormatter");

            var binFormatter = new BinaryFormatter();

            using (var file = new FileStream("person.bin", FileMode.OpenOrCreate))
            {
                binFormatter.Serialize(file, collection);
            }

            using (var file = new FileStream("person.bin", FileMode.OpenOrCreate))
            {
                var newPeople = binFormatter.Deserialize(file) as List<Person>;

                if (newPeople != null)
                {
                    foreach (var item in newPeople)
                    {
                        Console.WriteLine($"{item}{item.Character}");
                    }
                }
            }
        }

        public void SaveXmlSerializer(List<Person> collection)
        {
            Color("XmlSerializer");

            var xmlFormatter = new XmlSerializer(typeof(List<Person>));

            using (var file = new FileStream("person.xml", FileMode.OpenOrCreate))
            {
                xmlFormatter.Serialize(file, collection);
            }

            using (var file = new FileStream("person.xml", FileMode.OpenOrCreate))
            {
                var newPeople = xmlFormatter.Deserialize(file) as List<Person>;

                if (newPeople != null)
                {
                    foreach (var item in newPeople)
                    {
                        Console.WriteLine($"{item}{item.Character}");
                    }
                }
            }
        }

        public void SaveJsonSerializer(List<Character> collection)
        {
            Color("JsonSerializer");

            var jsonFormatter = new DataContractJsonSerializer(typeof(List<Character>));

            using (var file = new FileStream("characters.json", FileMode.OpenOrCreate))
            {
                jsonFormatter.WriteObject(file, collection);
            }

            using (var file = new FileStream("characters.json", FileMode.OpenOrCreate))
            {
                var newCharacter = jsonFormatter.ReadObject(file) as List<Character>;

                if (newCharacter != null)
                {
                    foreach (var item in newCharacter)
                    {
                        Console.WriteLine(item);
                    }
                }
            }
        }

        public void SaveSoapSerializer(List<Character> collection)
        {
            Color("SoapSerializer");

            var soapFormatter = new SoapFormatter();

            using (var file = new FileStream("characters.soap", FileMode.OpenOrCreate))
            {
                soapFormatter.Serialize(file, collection.ToArray());
            }

            using (var file = new FileStream("characters.soap", FileMode.OpenOrCreate))
            {
                var newCharacter = soapFormatter.Deserialize(file) as Character[];

                if (newCharacter != null)
                {
                    foreach (var item in newCharacter)
                    {
                        Console.WriteLine(item);
                    }
                }
            }
        }
    }
}
